import { arrayVisits } from "./getCardsArray.js";
import UserPost from "../classes/creatCards.js"

const container = document.querySelector(".user-posts");
const selectUrgency = document.querySelector(".select-urgency");
const selectStatus = document.querySelector(".select-status");
const searchInput = document.querySelector("#search-input");

export const searchCards = () => {
  container.innerHTML = "";

  if(selectUrgency.value === "all" && selectStatus.value === "all"){
    const filtredArrayVisits = arrayVisits.filter(({fullName, description}) => fullName.includes(searchInput.value) || description.includes(searchInput.value))
    new UserPost.creat(filtredArrayVisits)
  }

  else if(selectUrgency.value === "all" && selectStatus.value !== "all"){
    const filtredArrayVisits = arrayVisits.filter(({fullName, description, status}) => (fullName.includes(searchInput.value) || description.includes(searchInput.value)) && (status.includes(selectStatus.value))) 
    new UserPost.creat(filtredArrayVisits)
  }

  else if(selectUrgency.value !== "all" && selectStatus.value === "all"){
    const filtredArrayVisits = arrayVisits.filter(({fullName, description, urgency}) => (fullName.includes(searchInput.value) || description.includes(searchInput.value)) && (urgency.includes(selectUrgency.value))) 
    new UserPost.creat(filtredArrayVisits)
  }

  else{
    const filtredArrayVisits = arrayVisits.filter(({fullName, description, urgency, status}) => (fullName.includes(searchInput.value) || description.includes(searchInput.value)) && (urgency.includes(selectUrgency.value)) && (status.includes(selectStatus.value))) 
    new UserPost.creat(filtredArrayVisits)
  }
}

